module.exports = {
  addons: ['@storybook/addon-actions/register'],
  stories: ['../src/components/**/!(node_modules)/*.stories.[tj]s'],
};
