import React from 'react';

import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from 'styled-components';
import 'typeface-roboto';

const theme = createMuiTheme();

const StylesDecorator = (storyFn) => (
  <React.Fragment>
    <ThemeProvider theme={theme}>{storyFn()}</ThemeProvider>
  </React.Fragment>
);

export default StylesDecorator;
