import React from 'react';
import { addDecorator } from '@storybook/react';

import StylesDecorator from './stylesDecorator';

addDecorator(StylesDecorator);
