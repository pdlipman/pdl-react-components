import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import {
  AppBar,
  Divider,
  Drawer,
  IconButton,
  List,
  ListItem,
  ListItemText,
  ListSubheader,
  Toolbar,
  Typography,
} from '@material-ui/core';
import {
  ChevronLeft as ChevronLeftIcon,
  Menu as MenuIcon,
} from '@material-ui/icons';

const StyledPaper = styled.div`
  width: 90%;
`;

const StyledTitle = styled(Typography)`
  padding: 1vw;
`;

const PersistentDrawer = ({ menuItems, onClick, pathname, tabs, title }) => {
  const [open, setOpen] = useState(false);

  const handleClick = (value) => {
    setOpen(false);
    onClick(value);
  };
  return (
    <>
      <AppBar position="static">
        <Toolbar>
          <IconButton color="inherit" onClick={() => setOpen(true)}>
            <MenuIcon />
          </IconButton>
          <StyledTitle color="inherit" variant="h6">
            {title}
          </StyledTitle>
        </Toolbar>
      </AppBar>
      <Drawer
        anchor="left"
        onClose={() => setOpen(false)}
        open={open}
        PaperProps={{ component: StyledPaper }}
      >
        <List
          subheader={
            <ListSubheader component="div">
              <IconButton onClick={() => setOpen(false)}>
                <ChevronLeftIcon />
              </IconButton>
            </ListSubheader>
          }
        >
          <Divider />
          {tabs.map((tab) => (
            <ListItem
              button
              key={tab.key}
              onClick={() => handleClick(tab.value)}
              selected={tab.value === pathname}
            >
              <ListItemText primary={tab.label} />
            </ListItem>
          ))}
          <Divider />
          {menuItems}
        </List>
      </Drawer>
    </>
  );
};

PersistentDrawer.propTypes = {
  menuItems: PropTypes.arrayOf(PropTypes.element),
  onClick: PropTypes.func,
  pathname: PropTypes.string,
  tabs: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string,
      label: PropTypes.string,
      value: PropTypes.string,
    }),
  ),
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
};

PersistentDrawer.defaultProps = {
  menuItems: [],
  onClick: () => {},
  pathname: '',
  tabs: [],
  title: 'Persistent Drawer',
};
export default PersistentDrawer;
