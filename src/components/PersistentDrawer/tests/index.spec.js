import React from 'react';
import { render } from '@testing-library/react';

import PersistentDrawer from '..';

describe('PersistentDrawer', () => {
  it('renders', () => {
    const { asFragment } = render(<PersistentDrawer />);
    expect(asFragment()).toMatchSnapshot();
  });
});
