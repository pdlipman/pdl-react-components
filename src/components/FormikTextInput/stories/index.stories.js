import React from 'react';
import { Field, Formik } from 'formik';
import FormikTextInput from '..';

export default {
  component: FormikTextInput,
  title: 'FormikTextInput',
};

const fieldName = 'text';
const labelName = 'Formik Text Input';

export const Default = () => (
  <Formik initialValues={{ [fieldName]: '' }} onSubmit={() => {}}>
    <Field
      component={FormikTextInput}
      key={fieldName}
      label={labelName}
      name={fieldName}
    />
  </Formik>
);

export const WithValidation = () => (
  <Formik
    initialValues={{ [fieldName]: '' }}
    onSubmit={() => {}}
    validate={(values) => {
      const errors = { [fieldName]: '' };

      if (!values[fieldName]) {
        errors[fieldName] = 'Required.';
      }

      return errors;
    }}
  >
    <Field
      component={FormikTextInput}
      key={fieldName}
      label={labelName}
      name={fieldName}
    />
  </Formik>
);
