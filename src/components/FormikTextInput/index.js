import React from 'react';
import PropTypes from 'prop-types';
import { useField } from 'formik';

import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';

const FormikTextInput = ({ label, field }) => {
  const [childField, meta] = useField(field);
  const { name, onBlur, onChange, value } = childField;
  const error = !!(meta.error && meta.touched);
  return (
    <FormControl fullWidth>
      <InputLabel error={error}>{label}</InputLabel>
      <Input
        error={error}
        inputProps={{ 'data-testid': `text-input--input` }}
        name={name}
        onBlur={onBlur}
        onChange={onChange}
        placeholder=""
        type="text"
        value={value}
      />
      <FormHelperText error={error}>
        {meta.touched ? meta.error : undefined}
      </FormHelperText>
    </FormControl>
  );
};

FormikTextInput.propTypes = {
  field: PropTypes.shape({
    name: PropTypes.string.isRequired,
    onBlur: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    value: PropTypes.string.isRequired,
  }).isRequired,
  label: PropTypes.string,
};

FormikTextInput.defaultProps = {
  label: 'Text Input',
};

export default FormikTextInput;
