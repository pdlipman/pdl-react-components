/**
 *
 * Tests for TextInput
 *
 * @see https://github.com/react-boilerplate/react-boilerplate/tree/master/docs/testing
 *
 */

import { fireEvent, render, waitFor } from '@testing-library/react';
import { Field, Formik } from 'formik';
import React from 'react';
import FormikTextInput from '..';

describe('FormikTextInput', () => {
  test('should have validation error given input field is touched and error exists on form', async () => {
    const fieldName = 'text';
    const labelName = 'Text';

    const wrapper = render(
      <Formik
        initialValues={{ text: '' }}
        onSubmit={() => {}}
        validate={(values) => {
          const errors = { text: '' };

          if (!values.text) {
            errors.text = 'Required.';
          }

          return errors;
        }}
      >
        <Field
          component={FormikTextInput}
          key={fieldName}
          label={labelName}
          name={fieldName}
        />
      </Formik>,
    );
    const { getByTestId, findByText } = wrapper;

    const input = getByTestId(`text-input--input`);
    fireEvent.blur(input);

    expect(findByText('Required.')).not.toBeNull();

    fireEvent.change(input, { target: { value: 'test' } });
    expect(input.getAttribute('value')).toBe('test');
  });
});
