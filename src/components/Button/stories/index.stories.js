import React from 'react';
import { action } from '@storybook/addon-actions';

import Button from '..';

export default {
  component: Button,
  title: 'Button',
};

export const Default = () => (
  <Button onClick={action('button-click')}>Hello World!</Button>
);
