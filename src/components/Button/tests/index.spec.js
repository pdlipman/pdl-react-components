import React from 'react';
import { render } from '@testing-library/react';

import Button from '..';

describe('Button', () => {
  it('renders', () => {
    const { asFragment } = render(<Button onClick={() => {}}>Test</Button>);
    expect(asFragment()).toMatchSnapshot();
  });
});
