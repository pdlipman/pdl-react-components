import React from 'react';
import { CssBaseline } from '@material-ui/core';

import Layout from '..';

export default {
  component: Layout,
  title: 'Layout',
  decorators: [
    (storyFn) => (
      <>
        <CssBaseline>{storyFn()}</CssBaseline>
      </>
    ),
  ],
};

export const Default = () => (
  <Layout>
    <Layout.Header>Test Header</Layout.Header>
    <Layout.Content>Test Content</Layout.Content>
    <Layout.Footer>Test Footer</Layout.Footer>
  </Layout>
);
