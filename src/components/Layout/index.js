import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const StyledLayout = styled.div`
  min-height: 100vh;
  position: relative;
`;

const StyledContent = styled.div`
  min-height: 100vh;
  padding-bottom: 10vh;
`;

const StyledFooter = styled.footer`
  bottom: 0;
  height: 10vh;
  position: absolute;
  width: 100%;
`;

const Header = ({ children }) => <>{children}</>;
Header.propTypes = { children: PropTypes.node };
Header.defaultProps = { children: null };

const Content = ({ children }) => <>{children}</>;
Content.propTypes = { children: PropTypes.node };
Content.defaultProps = { children: null };

const Footer = ({ children }) => <>{children}</>;
Footer.propTypes = { children: PropTypes.node };
Footer.defaultProps = { children: null };

const Layout = ({ children }) => {
  const childArray = React.Children.toArray(children);

  const getElements = (type) => {
    const elements = childArray.find((child) => child.type === type);
    return elements;
  };

  const header = getElements(Header);
  const content = getElements(Content);
  const footer = getElements(Footer);

  return (
    <StyledLayout>
      <StyledContent>
        <header>{header}</header>
        {content}
      </StyledContent>
      <StyledFooter>{footer}</StyledFooter>
    </StyledLayout>
  );
};

Layout.propTypes = {
  children: (props, propName) => {
    const prop = props[propName];
    const allowedComponents = [Header, Content, Footer];
    const childArray = React.Children.toArray(prop);

    let error = null;
    allowedComponents.forEach((component) => {
      if (childArray.filter((child) => child.type === component).length > 1) {
        error = new Error(
          'Child components should only include one Layout.Header, one Layout.Content, and one Layout.Footer',
        );
      }
    });
    childArray.forEach((child) => {
      if (!allowedComponents.includes(child.type)) {
        error = new Error(
          'Child components should only include one Layout.Header, one Layout.Content, and one Layout.Footer',
        );
      }
    });
    return error;
  },
};

Layout.defaultProps = { children: [] };

Layout.Header = Header;
Layout.Content = Content;
Layout.Footer = Footer;

export default Layout;
