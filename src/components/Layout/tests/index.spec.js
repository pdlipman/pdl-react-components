import React from 'react';
import { render } from '@testing-library/react';

import Layout from '..';

describe('Layout', () => {
  it('renders', () => {
    const { asFragment } = render(<Layout />);
    expect(asFragment()).toMatchSnapshot();
  });
});
