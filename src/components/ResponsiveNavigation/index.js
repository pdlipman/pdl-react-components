import React from 'react';
import PropTypes from 'prop-types';

import { useTheme } from '@material-ui/core/styles';
import { useMediaQuery } from '@material-ui/core';

import NavigationBar from '../NavigationBar';
import PersistentDrawer from '../PersistentDrawer';

const ResponsiveNavigation = ({
  menuItems,
  onClick,
  pathname,
  tabs,
  title,
}) => {
  const theme = useTheme();
  const isDesktop = useMediaQuery(theme.breakpoints.up('sm'));
  if (isDesktop) {
    return (
      <NavigationBar
        menuItems={menuItems}
        onClick={onClick}
        pathname={pathname}
        tabs={tabs}
        title={title}
      />
    );
  }

  return (
    <PersistentDrawer
      menuItems={menuItems}
      onClick={onClick}
      pathname={pathname}
      tabs={tabs}
      title={title}
    />
  );
};

ResponsiveNavigation.propTypes = {
  menuItems: PropTypes.arrayOf(PropTypes.element),
  onClick: PropTypes.func,
  pathname: PropTypes.string,
  tabs: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string,
      label: PropTypes.string,
      value: PropTypes.string,
    }),
  ),
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
};

ResponsiveNavigation.defaultProps = {
  menuItems: [],
  onClick: () => {},
  pathname: '',
  tabs: [],
  title: 'Persistent Drawer',
};
export default ResponsiveNavigation;
