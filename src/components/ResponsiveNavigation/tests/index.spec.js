import React from 'react';
import { render } from '@testing-library/react';

import ResponsiveNavigation from '..';

describe('ResponsiveNavigation', () => {
  it('renders', () => {
    const { asFragment } = render(<ResponsiveNavigation />);
    expect(asFragment()).toMatchSnapshot();
  });
});
