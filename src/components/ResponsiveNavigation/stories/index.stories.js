import React from 'react';
import { action } from '@storybook/addon-actions';

import { IconButton } from '@material-ui/core';
import { AccountCircle } from '@material-ui/icons';

import ResponsiveNavigation from '..';

const menuItems = [
  <>
    <IconButton
      color="inherit"
      data-testid="user-menu--toggle"
      onClick={action('menu-click')}
    >
      <AccountCircle />
    </IconButton>
  </>,
];

const pathname = '/';
const tabs = [
  {
    key: 'home',
    label: 'Home',
    value: '/',
  },
  {
    key: 'about',
    label: 'About',
    value: '/about',
  },
];

const title = 'Storybook';

export default {
  component: ResponsiveNavigation,
  title: 'ResponsiveNavigation',
};

export const Default = () => (
  <ResponsiveNavigation pathname={pathname} tabs={tabs} title={title} />
);

export const WithMenuItems = () => (
  <ResponsiveNavigation
    menuItems={menuItems}
    pathname={pathname}
    tabs={tabs}
    title={title}
  />
);
