import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { AppBar, Tab, Tabs, Toolbar, Typography } from '@material-ui/core';

const StyledTabs = styled(Tabs)`
  flex-grow: 1;
`;

const StyledTitle = styled(Typography)`
  padding: 1vw;
`;

const NavigationBar = ({ menuItems, onClick, pathname, tabs, title }) => {
  const selectedTab = tabs.find((tab) => tab.value === pathname);
  const tabsValue = selectedTab ? selectedTab.value : false;

  const handleClick = (value) => {
    onClick(value);
  };

  return (
    <AppBar position="static">
      <Toolbar>
        <StyledTitle variant="h6" color="inherit">
          {title}
        </StyledTitle>
        <StyledTabs value={tabsValue}>
          {tabs.map((tab) => (
            <Tab
              key={tab.key}
              label={tab.label}
              onClick={() => handleClick(tab.value)}
              value={tab.value}
            />
          ))}
        </StyledTabs>
        {menuItems}
      </Toolbar>
    </AppBar>
  );
};

NavigationBar.propTypes = {
  menuItems: PropTypes.arrayOf(PropTypes.element),
  onClick: PropTypes.func,
  pathname: PropTypes.string,
  tabs: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string,
      label: PropTypes.string,
      value: PropTypes.string,
    }),
  ),
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
};

NavigationBar.defaultProps = {
  menuItems: [],
  onClick: () => {},
  pathname: '',
  tabs: [],
  title: 'Navigation Bar',
};

export default NavigationBar;
