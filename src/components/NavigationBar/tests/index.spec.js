import React from 'react';
import { render } from '@testing-library/react';

import NavigationBar from '..';

describe('NavigationBar', () => {
  it('renders', () => {
    const { asFragment } = render(<NavigationBar />);
    expect(asFragment()).toMatchSnapshot();
  });
});
