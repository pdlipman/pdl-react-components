import { fireEvent, render, waitFor } from '@testing-library/react';
import { Field, Formik } from 'formik';
import React from 'react';
import FormikPasswordInput from '../index';

describe('FormikPasswordInput', () => {
  test('should have validation error given input field is touched and error exists on form', async () => {
    const fieldName = 'password';
    const labelName = 'Password';

    const wrapper = render(
      <Formik
        initialValues={{ password: '' }}
        onSubmit={() => {}}
        validate={(values) => {
          const errors = { password: '' };

          if (!values.password) {
            errors.password = 'Required.';
          }

          return errors;
        }}
      >
        <Field
          component={FormikPasswordInput}
          key={fieldName}
          label={labelName}
          name={fieldName}
        />
      </Formik>,
    );
    const { getByTestId, findByText } = wrapper;

    const input = getByTestId(`password-input--input`);
    fireEvent.blur(input);

    expect(findByText('Required.')).not.toBeNull();

    fireEvent.change(input, { target: { value: 'test' } });
    expect(input.getAttribute('value')).toBe('test');
  });

  test('toggle visibility', async () => {
    const fieldName = 'password';
    const labelName = 'Password';

    const wrapper = render(
      <Formik initialValues={{ password: '' }} onSubmit={() => {}}>
        <Field
          component={FormikPasswordInput}
          key={fieldName}
          label={labelName}
          name={fieldName}
        />
      </Formik>,
    );
    const { getByTestId } = wrapper;

    const input = getByTestId(`password-input--input`);
    expect(input.getAttribute('type')).toBe('password');
    const toggle = getByTestId('password-input--visibility-toggle');
    fireEvent.click(toggle);
    expect(input.getAttribute('type')).toBe('text');
  });
});
