import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import { useField } from 'formik';

import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';
import InputLabel from '@material-ui/core/InputLabel';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

const FormikPasswordInput = ({ label, field }) => {
  const [childField, meta] = useField(field);
  const { name, onBlur, onChange, value } = childField;

  const inputRef = useRef();
  const [selectionStart, setSelectionStart] = useState();
  const [isPasswordShown, setIsPasswordShown] = useState(false);

  const handleClickShowPassword = () => {
    setIsPasswordShown(!isPasswordShown);
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const error = !!(meta.error && meta.touched);

  const handleSelect = () => {
    const input = inputRef.current;
    if (input) {
      setSelectionStart(input.selectionStart);
    }
  };

  useEffect(() => {
    const input = inputRef.current;
    if (input) {
      input.selectionStart = selectionStart;
    }
  }, [isPasswordShown]);

  return (
    <FormControl fullWidth>
      <InputLabel error={error}>{label}</InputLabel>
      <Input
        inputProps={{ 'data-testid': `password-input--input` }}
        endAdornment={
          <InputAdornment position="end">
            <IconButton
              data-testid="password-input--visibility-toggle"
              aria-label="toggle password visibility"
              onClick={handleClickShowPassword}
              onMouseDown={handleMouseDownPassword}
            >
              {isPasswordShown ? (
                <Visibility color={error ? 'error' : 'action'} />
              ) : (
                <VisibilityOff color={error ? 'error' : 'action'} />
              )}
            </IconButton>
          </InputAdornment>
        }
        error={error}
        name={name}
        onBlur={onBlur}
        onChange={onChange}
        onSelect={handleSelect}
        placeholder=""
        type={isPasswordShown ? 'text' : 'password'}
        inputRef={inputRef}
        value={value}
      />
      <FormHelperText data-testid="password-input--errors" error={error}>
        {meta.touched ? meta.error : undefined}
      </FormHelperText>
    </FormControl>
  );
};

FormikPasswordInput.propTypes = {
  field: PropTypes.shape({
    name: PropTypes.string.isRequired,
    onBlur: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    value: PropTypes.string.isRequired,
  }).isRequired,
  label: PropTypes.string,
};

FormikPasswordInput.defaultProps = {
  label: 'Password Input',
};

export default FormikPasswordInput;
