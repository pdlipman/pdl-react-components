import React from 'react';
import { Field, Formik } from 'formik';
import FormikPasswordInput from '..';

export default {
  component: FormikPasswordInput,
  title: 'FormikPasswordInput',
};

const fieldName = 'password';
const labelName = 'Formik Password Input';

export const Default = () => (
  <Formik initialValues={{ [fieldName]: '' }} onSubmit={() => {}}>
    <Field
      component={FormikPasswordInput}
      key={fieldName}
      label={labelName}
      name={fieldName}
    />
  </Formik>
);

export const WithValidation = () => (
  <Formik
    initialValues={{ [fieldName]: '' }}
    onSubmit={() => {}}
    validate={(values) => {
      const errors = { [fieldName]: '' };

      if (!values[fieldName]) {
        errors[fieldName] = 'Required.';
      }

      return errors;
    }}
  >
    <Field
      component={FormikPasswordInput}
      key={fieldName}
      label={labelName}
      name={fieldName}
    />
  </Formik>
);
