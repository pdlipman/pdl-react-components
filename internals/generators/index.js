const path = require('path');
const { execSync } = require('child_process');

const componentGenerator = require('./component/index.js');

module.exports = (plop) => {
  plop.setGenerator('component', componentGenerator);

  plop.setActionType('prettify', (answers, config) => {
    const folderPath = `${path.join(
      __dirname,
      '/../../',
      config.path,
      plop.getHelper('properCase')(answers.name),
      '**',
      '**.{js,json}',
    )}`;

    execSync(`yarn prettier --write "${folderPath}"`);
    return folderPath;
  });
};
