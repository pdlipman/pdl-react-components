module.exports = {
  transform: {
    '\\.(js|jsx)?$': 'babel-jest',
  },
  testRegex: '(/*/.*.spec).(js?|jsx?)$',
  moduleFileExtensions: ['js', 'jsx', 'json', 'node'],
  testPathIgnorePatterns: [
    '/node_modules/',
    '/public/',
    '/dist/',
    '/build/',
    '/stories/',
    '/.git/',
  ],
  setupFilesAfterEnv: ['@testing-library/jest-dom/extend-expect'],
  verbose: true,
  collectCoverage: true,
  coverageDirectory: './coverage/',
  collectCoverageFrom: [
    'src/**/*.{js,jsx}',
    '!src/**/*.spec.{js,jsx}',
    '!src/**/*.stories.{js,jsx}',
  ],
};
